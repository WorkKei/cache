package com.test.da;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

//@SpringBootApplication
//@DependsOn(value = {"redisServer"})
public class RedisReactiveManyApplication {

    private final ReactiveRedisConnectionFactory factory;
    private final ReactiveRedisOperations<String, String> reactiveRedisOperations;
    private static final AtomicInteger count = new AtomicInteger(0);

    public RedisReactiveManyApplication(ReactiveRedisConnectionFactory factory, ReactiveRedisOperations<String, String> reactiveRedisOperations) {
        this.factory = factory;
        this.reactiveRedisOperations = reactiveRedisOperations;
    }

//    @PostConstruct
    public void loadData() {
        List<String> data = new ArrayList<>();
        IntStream.range(0, 100).forEach(i -> data.add(UUID.randomUUID().toString()));
//        IntStream.range(0, 100000).forEach(i -> data.add(UUID.randomUUID().toString()));
        Flux<String> stringFlux = Flux.fromIterable(data);
        factory.getReactiveConnection().serverCommands().flushAll().thenMany(stringFlux.flatMap(uid -> reactiveRedisOperations.opsForValue().set(String.valueOf(count.getAndAdd(1)), uid))).subscribe();
    }
}
