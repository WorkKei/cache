package com.test.da.controller;

import com.test.da.constants.Constants;
import com.test.da.vo.redis.Color;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

//@RestController
public class RedisReactiveController {

    private final ReactiveRedisOperations<String, Color> reactiveRedisOperations;

    public RedisReactiveController(ReactiveRedisOperations<String, Color> reactiveRedisOperations) {
        this.reactiveRedisOperations = reactiveRedisOperations;
    }

    @GetMapping("/colors4")
    private Mono<List<Color>> findAllByMRedisReactiveAutoConfigurationultiGet() {
        return reactiveRedisOperations
                .opsForValue()
                .multiGet(Constants.colorList.stream().map(k -> "colors::v4::" + k).collect(Collectors.toList()));
    }
}
