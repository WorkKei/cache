package com.test.da.controller;

import com.test.da.constants.Constants;
import com.test.da.service.TestService;
import com.test.da.vo.ColorVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TestController {

    private final TestService testService;

    public TestController(TestService testService) {
        this.testService = testService;
    }

    @ApiOperation(value = "H2 직접 호출 테스트")
    @GetMapping("/h2")
    public List<ColorVO> h2() {
        List<ColorVO> colorVOS = new ArrayList<>();
        Constants.colorList.forEach(colorName -> {
            colorVOS.add(testService.findByName(colorName));
        });
        return colorVOS;
    }

    @GetMapping("/cache")
    public List<ColorVO> redisSpringCache() {
        List<ColorVO> colorVOS = new ArrayList<>();
        Constants.colorList.forEach(colorName -> {
            colorVOS.add(testService.findByNameCache(colorName));
        });
        return colorVOS;
    }

    @GetMapping("/data")
    private List<ColorVO> springRedisData() {
        List<ColorVO> colorVOS = new ArrayList<>();
        Constants.colorList.forEach(colorName -> {
            colorVOS.add(testService.findByNameData(colorName));
        });
        return colorVOS;
    }

    @GetMapping("/mGet")
    private List<ColorVO> springRedisTemplete() {
        return testService.findAllMGet();
    }
}
