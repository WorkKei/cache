package com.test.da.controller;

import com.test.da.repository.redis.PointRedisRepository;
import com.test.da.vo.redis.Point;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController(value = "/redis")
public class RedisTestController {

    @Autowired
    private PointRedisRepository pointRedisRepository;

    @ApiOperation(value = "레디스테스트")
    @GetMapping("/redisTest")
    public String redisTest() {
        System.out.println("redisTest !!!!!!!!!!!!!!!!!!!");
        return "redisTest OK";
    }

    @ApiOperation(value = "레디스 등록 조회 테스트")
    @GetMapping("/redisInsertNSelect")
    public @ResponseBody
    Point redisInsertNSelect() {

        String id = "ghjoTest";
        LocalDateTime refreshTime = LocalDateTime.of(2020, 3, 30, 0, 0);
        Point point = Point.builder()
                .id(id)
                .amount(1000L)
                .refreshTime(refreshTime)
                .build();

        pointRedisRepository.save(point);

        Point savedPoint = pointRedisRepository.findById(id).get();

        return savedPoint;
    }
}
