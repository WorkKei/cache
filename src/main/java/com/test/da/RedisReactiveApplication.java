package com.test.da;

import com.test.da.constants.Constants;
import com.test.da.vo.redis.Color;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;

//@SpringBootApplication
//@DependsOn(value = {"redisServer"})
public class RedisReactiveApplication {

    private final ReactiveRedisConnectionFactory factory;
    private final ReactiveRedisOperations<String, Color> reactiveRedisOperations;

    public RedisReactiveApplication(ReactiveRedisConnectionFactory factory, ReactiveRedisOperations<String, Color> reactiveRedisOperations) {
        this.factory = factory;
        this.reactiveRedisOperations = reactiveRedisOperations;
    }

//    @PostConstruct
    public void loadData() {
        factory.getReactiveConnection().serverCommands().info().thenMany(
                Flux.fromIterable(Constants.colorList)
                    .map(name -> new Color(
                            name
                            , javafx.scene.paint.Color.web(name).getRed()
                            , javafx.scene.paint.Color.web(name).getBlue()
                            , javafx.scene.paint.Color.web(name).getGreen()))
                    .flatMap(color -> reactiveRedisOperations.opsForValue().set("colors::v4::" + color.getName(), color)))
                .subscribe(null, error -> System.out.println("에러"), () -> System.out.println("추가 데이터 저장 완료"));
    }

}
