package com.test.da.repository.redis;

import com.test.da.vo.ColorVO;
import org.springframework.data.repository.CrudRepository;

public interface ColorVORepository extends CrudRepository<ColorVO, String> {
}
