package com.test.da.repository.redis;

import com.test.da.vo.redis.Point;
import org.springframework.data.repository.CrudRepository;

public interface PointRedisRepository extends CrudRepository<Point, String> {
}
