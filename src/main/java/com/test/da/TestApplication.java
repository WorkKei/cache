package com.test.da;

import com.test.da.constants.Constants;
import com.test.da.repository.redis.ColorVORepository;
import com.test.da.vo.ColorVO;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

//@EnableCaching // for Spring Cache
@SpringBootApplication
@EnableRedisRepositories(basePackages = "com.test.da.repository.redis") // for Spring Data Redis
public class TestApplication implements CommandLineRunner {

//    private final H2ColorRepository h2ColorRepository; // for h2

    private final ColorVORepository colorVORepository;

    private final RedisTemplate redisTemplate;

    public TestApplication(ColorVORepository colorVORepository, RedisTemplate redisTemplate) {
        // for h2
//    public TestApplication(H2ColorRepository h2ColorRepository, ColorVORepository colorVORepository) {
//        this.h2ColorRepository = h2ColorRepository;
        this.colorVORepository = colorVORepository;
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void run(String... args) throws Exception {
        // JacksonJsonRedisSerializer // for redisTemplate
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(ColorVO.class));

        // for h2
//        System.out.println("=================== set h2 =========================");
//        Constants.colorList.stream().forEach(name -> h2ColorRepository.save(new ColorVO(name
//                , javafx.scene.paint.Color.web(name).getRed()
//                , javafx.scene.paint.Color.web(name).getBlue()
//                , javafx.scene.paint.Color.web(name).getGreen())));

        System.out.println("=================== set redis =========================");
//        Constants.colorList.stream().forEach(name -> colorVORepository.save(new ColorVO(name // for Spring Data Redis
        Constants.colorList.stream().forEach(name -> redisTemplate.opsForValue().set("colors::v1:" + name, new ColorVO(name // redisTemplate
                , javafx.scene.paint.Color.web(name).getRed()
                , javafx.scene.paint.Color.web(name).getBlue()
                , javafx.scene.paint.Color.web(name).getGreen())));
    }
}
