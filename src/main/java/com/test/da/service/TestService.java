package com.test.da.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.da.constants.Constants;
import com.test.da.repository.redis.ColorVORepository;
import com.test.da.vo.ColorVO;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TestService {

//    private final H2ColorRepository h2ColorRepository; // for h2

    private final ColorVORepository colorVORepository;

    private final RedisTemplate redisTemplate;

    public TestService(ColorVORepository colorVORepository, RedisTemplate redisTemplate) {
        // for h2
//    public TestService(H2ColorRepository h2ColorRepository, ColorVORepository colorVORepository) {
//        this.h2ColorRepository = h2ColorRepository;
        this.colorVORepository = colorVORepository;
        this.redisTemplate = redisTemplate;
    }

    // for h2
//    public List<ColorVO> getFindAll() {
//        return h2ColorRepository.findAll();
//    }

    public ColorVO findByName(String name) {
//        return h2ColorRepository.findById(name).orElse(null); // for h2
        return null;
    }

//    @Cacheable(value = "colors::v1", key = "#name") // for h2 with redis spring cache
    public ColorVO findByNameCache(String name) {
//        return h2ColorRepository.findById(name).orElse(null); // for h2
        return null;
    }

    public ColorVO findByNameData(String colorName) {
        return colorVORepository.findById(colorName).orElse(null);
    }

    public List<ColorVO> findAllMGet() {
        ObjectMapper objectMapper = new ObjectMapper();
        List<byte[]> keys = Constants.colorList.stream()
                .map(k -> ("colors::v1:" + k).getBytes())
                .collect(Collectors.toList());
        return redisTemplate.getConnectionFactory().getConnection()
                .mGet(keys.stream().toArray(byte[][]::new))
                .parallelStream()
                .map(colorByte -> {
                    try {
                        return objectMapper.readValue(new String(colorByte), ColorVO.class);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return null;
                }).collect(Collectors.toList());
    }
}
