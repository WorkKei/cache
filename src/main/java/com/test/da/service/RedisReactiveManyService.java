package com.test.da.service;

import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

//@Service("redisReactiveManyService")
public class RedisReactiveManyService {

//    private final ReactiveRedisConnectionFactory factory;
//    private final ReactiveRedisOperations<String, String> reactiveRedisOperations;
//    private final RedisTemplate<String, String> stringStringRedisTemplate;
//    private static final AtomicInteger count = new AtomicInteger(0);
//
//    public RedisReactiveManyService(ReactiveRedisConnectionFactory factory, ReactiveRedisOperations<String, String> reactiveRedisOperations, RedisTemplate<String, String> stringStringRedisTemplate) {
//        this.factory = factory;
//        this.reactiveRedisOperations = reactiveRedisOperations;
//        this.stringStringRedisTemplate = stringStringRedisTemplate;
//    }
//
//    public void loadData() {
////        List<String> data = new ArrayList<>();
////        IntStream.range(0, 100000).forEach(i -> data.add(UUID.randomUUID().toString()));
////        Flux<String> stringFlux = Flux.fromIterable(data);
////        factory.getReactiveConnection().serverCommands().flushAll().thenMany(stringFlux.flatMap(uid -> reactiveRedisOperations.opsForValue().set(String.valueOf(count.getAndAdd(1)), uid))).subscribe();
//    }
//
//    public Flux<String> findReactorList() {
//        return reactiveRedisOperations.keys("*").flatMap(key -> reactiveRedisOperations.opsForValue().get(key));
//    }
//
//    public Flux<String> findNormalList() {
//        return Flux.fromIterable(Objects.requireNonNull(stringStringRedisTemplate.keys("*")).stream().map(key -> stringStringRedisTemplate.opsForValue().get(key)).collect(Collectors.toList()));
//    }
//
//    public List<String> findStringList() {
//        return stringStringRedisTemplate.getConnectionFactory().getConnection()
//                .mGet(Objects.requireNonNull(stringStringRedisTemplate.keys("*")).stream().map(k -> k.getBytes()).collect(Collectors.toList()).stream().toArray(byte[][]::new))
////                .parallelStream()
//                .stream()
//                .map(b -> new String(b))
//                .collect(Collectors.toList());
//    }

}
