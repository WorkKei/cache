package com.test.da.config;

import com.zaxxer.hikari.HikariDataSource;
import org.h2.tools.Server;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@Profile("local")
public class H2Config {

    // in memory
//    @Bean("h2TcpServer") // for h2
    public Server h2TcpServer() throws SQLException {
        return Server.createTcpServer().start();
    }

    // with file
    // connect /url only /no user/ jdbc:h2:tcp://localhost:9093/external_db_name
    /**
     * @see org.h2.server.TcpServer
     * @return
     * @throws SQLException
     */
//    @Bean
//    @ConfigurationProperties("spring.datasource.hikari")
    public DataSource dataSource() throws SQLException {
        Server server = adviceRun(9093, "external_db_name", "dbname", FilePath.relative);
//        Server server = defaultRun(9093);
        if(server.isRunning(true)){
            System.out.println("server run success");
        }
        System.out.println("h2 server url = " + server.getURL());

        return new HikariDataSource();
    }

    private Server adviceRun(int port, String externalDbName, String dbname, FilePath db_store) throws SQLException {
        return Server.createTcpServer(
                "-tcp",
                "-tcpAllowOthers",
                "-ifNotExists",
                "-tcpPort", port+"", "-key", externalDbName, db_store.value2(dbname)).start();
    }

    private Server defaultRun(int port) throws SQLException {
        return Server.createTcpServer(
                "-tcp",
                "-tcpAllowOthers",
                "-ifNotExists",
                "-tcpPort", port+"").start();
    }

    enum FilePath {
        absolute("~/"),
        relative("./");
        String prefix;
        FilePath(String prefix){
            this.prefix = prefix;
        }
        public String value2(String dbname){
            String path = "src/main/resources/db/";
            return prefix + path + dbname;
        }
    }


}
