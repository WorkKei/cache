package com.test.da.vo;

import lombok.*;
import org.springframework.data.annotation.Id; // for spring redis data
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Entity;
//import javax.persistence.Id; // for h2
import java.io.Serializable;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
//@Entity // for h2
@RedisHash("colors::v1:") // for spring redis data - redis type hash
public class ColorVO implements Serializable {

    @Id
    private String name;

    private double red;
    private double blue;
    private double green;

}
