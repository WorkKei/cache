package com.test.da;

import com.test.da.repository.redis.PointRedisRepository;
import com.test.da.vo.redis.Point;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.time.LocalDateTime;

@SpringBootTest
public class RedisTest1 {

    @Autowired
    private PointRedisRepository pointRedisRepository;

    @Test
    public void 기본_등록_조회기능() {
        // given
        String id = "ghjoTest";
        LocalDateTime refreshTime = LocalDateTime.of(2020, 3, 30, 0, 0);
        Point point = Point.builder()
                .id(id)
                .amount(1000L)
                .refreshTime(refreshTime)
                .build();

        // when
        pointRedisRepository.save(point);

        // then
        Point savedPoint = pointRedisRepository.findById(id).get();
        System.out.println("savedPoint > " + savedPoint);
        assertThat(savedPoint.getAmount(), is(equalTo(1000L)));
        assertThat(savedPoint.getRefreshTime(), is(equalTo(refreshTime)));

    }

    @Test
    public void 수정기능() {
        //given
        String id = "ghjoTest";
        LocalDateTime refreshTime = LocalDateTime.of(2020, 3, 30, 0, 0);
        pointRedisRepository.save(Point.builder().id(id).amount(1000L).refreshTime(refreshTime).build());

        //when
        Point savedPoint = pointRedisRepository.findById(id).get();
        savedPoint.refresh(2000L, LocalDateTime.of(2020, 6, 1, 0, 0));
        pointRedisRepository.save(savedPoint);

        //then
        Point refreshPoint = pointRedisRepository.findById(id).get();
        assertThat(refreshPoint.getAmount(), is(equalTo(2000L)));
//        assertThat(refreshPoint.getAmount(), is(equalTo(3000L)));

    }

    @Test
    public void 스프링부트junit5테스트() {
        System.out.println("테스트 성공!!!!!!!!!!");
        System.out.println("테스트 성공!!!!!!!!!!");
        System.out.println("테스트 성공!!!!!!!!!!");
        System.out.println("테스트 성공!!!!!!!!!!");
        System.out.println("테스트 성공!!!!!!!!!!");
        System.out.println("테스트 성공!!!!!!!!!!");
    }
}
